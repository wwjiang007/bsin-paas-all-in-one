package me.flyray.bsin.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

/**
 * Security 配置属性
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "security")
public class SecurityProperties {

    /**
     * 服务白名单
     */
    private Set<String> serviceWhiteList;


    /**
     * 方法白名单
     */
    private Set<String> methodWhiteList;


    /**
     * 服务-方法名白名单
     * <pre>
     *     ps:服务名:方法名
     *     eg: HelloService:sayHi
     * </pre>
     */
    private Set<String> serviceMethodList;
}
