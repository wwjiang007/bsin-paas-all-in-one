package me.flyray.bsin.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import io.seata.saga.engine.StateMachineEngine;
import me.flyray.bsin.server.context.DecisionEngineContextBuilder;

/**
 * @author ：bolei
 * @date ：Created in 2021/11/30 16:00
 * @description：bsin 脚手架启动类
 * @modified By：
 */

@ImportResource({"classpath*:sofa/rpc-provider-hello.xml"})
@SpringBootApplication
@MapperScan("me.flyray.bsin.server.mapper")
@ComponentScan("me.flyray.bsin.*")
@EnableScheduling //开启定时任务
public class BsinIntelligentDecisionApplication {

    public static void main(String[] args) {

        ApplicationContext applicationContext = SpringApplication.run(BsinIntelligentDecisionApplication.class, args);
        DecisionEngineContextBuilder decisionEngineContextBuilder = (DecisionEngineContextBuilder) applicationContext.getBean("decisionEngineContextBuilder");
        // 应用启动的时候，加载数据库中规则在规则库中
        decisionEngineContextBuilder.decisionEngineInitial();
    }

}
