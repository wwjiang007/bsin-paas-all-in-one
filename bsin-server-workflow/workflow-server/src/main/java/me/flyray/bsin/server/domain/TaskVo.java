package me.flyray.bsin.server.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.identitylink.service.impl.persistence.entity.IdentityLinkEntity;
import org.flowable.task.api.DelegationState;
import org.flowable.task.service.impl.persistence.entity.TaskEntityImpl;
import org.flowable.variable.service.impl.persistence.entity.VariableInstanceEntity;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskVo {
    //任务id
    public String taskId;

    public String assignee;

    public String processDefinitionId;

    public String tenantId;

    // 父任务id
    public String parentTaskId;

    // 任务名称
    public String name;

    // 流程实例id
    public String processInstanceId;

    // 医嘱
    public String medicalAdvice;




}
