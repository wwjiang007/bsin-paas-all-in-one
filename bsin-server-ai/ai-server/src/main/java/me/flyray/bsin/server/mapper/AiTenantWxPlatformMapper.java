package me.flyray.bsin.server.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import me.flyray.bsin.server.domain.AiModel;
import me.flyray.bsin.server.domain.TenantWxPlatform;

/**
 * @author bolei
 * @description 针对表【ai_tenant_wxmp】的数据库操作Mapper
 * @createDate 2023-04-25 18:41:19
 * @Entity generator.domain.AiTenantWxmp
 */
@Mapper
@Repository
public interface AiTenantWxPlatformMapper {

    TenantWxPlatform selectByAppId(String appId);

    TenantWxPlatform selectByCorpAgentId(String corpId, String agentId);

    void insert(TenantWxPlatform tenantWxPlatform);

    void deleteById(String serialNo);

    void updateById(TenantWxPlatform tenantWxPlatform);

    AiModel selectBySerialNo(String serialNo);

    TenantWxPlatform selectByTenantId(String tenantId);

}
