package me.flyray.bsin.facade.service;


import java.util.Map;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;


/**
* @author bolei
* @description 针对表【ai_tenant_wxmp_user_tag】的数据库操作Service
* @createDate 2023-04-28 12:46:58
*/

@Path("tenantWxmpUserTagService")
public interface TenantWxPlatformUserTagService {

    /**
     *添加
     */
    @POST
    @Path("add")
    @Produces("application/json")
    Map<String,Object> add(Map<String,Object> requestMap);

    /**
     *详情
     */
    @POST
    @Path("detail")
    @Produces("application/json")
    Map<String,Object> detail(Map<String,Object> requestMap);

}
