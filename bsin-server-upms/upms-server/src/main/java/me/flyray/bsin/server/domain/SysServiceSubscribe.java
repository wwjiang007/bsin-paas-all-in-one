package me.flyray.bsin.server.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;
import me.flyray.bsin.validate.AddGroup;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;

/**
 * @TableName sys_service_subscribe
 */
@Data
@TableName(value = "sys_service_subscribe")
public class SysServiceSubscribe implements Serializable {
  /** */
  @TableId private String id;

  /** 租户id */
  @NotBlank(
      message = "租户ID不能为空！",
      groups = {AddGroup.class})
  private String tenantId;

  /** 商户号 */
  @NotBlank(
      message = "商户号不能为空！",
      groups = {AddGroup.class})
  private String merchantNo;

  /** 客户号 */
  @TableId private String customerNo;

  /** 类型： 1、应用(服务) 2、功能 */
  private String type;

  /** 应用|功能 ID */
  private String typeId;

  /** 应用|功能状态：0、待缴费 1、正常 2、欠费停止 3、冻结 */
  private String status;

  /** 开始时间 */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime startTime;

  /** 结束时间 */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime endTime;
}
